const router = require('express').Router();
const {
    createUser,
    loginUser,
    emailVerification,
    refreshAccessToken,
    forgotEmail,
    passwordResetForm,
    passwordReset
} = require('../controllers/auth');

router.post('/register', createUser);
router.post('/login', loginUser);
router.post('/token/refresh', refreshAccessToken);
router.get('/verification/:token', emailVerification);
router.post('/forgot-password', forgotEmail);
router.get('/password/reset/:token', passwordResetForm);
router.post('/password/reset', passwordReset);

module.exports = router;


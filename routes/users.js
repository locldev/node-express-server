const router = require('express').Router();
const auth = require('../middleware/auth');
const userController = require('../controllers/user');

router.post('/me',auth,userController.getMyData);
module.exports = router;
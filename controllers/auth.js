const { User, validate } = require('../models/user');
const bcrypt = require("bcrypt");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const mailer = require('../services/mail');
const template = require('../templates/en');
const Blacklist = require('../models/blacklist');

const createUser = async (req, res) => {
    try {
        const { error } = validate(req.body);
        if (error) {
            return res.status(400).json({ message: error.details[0].message });
        }
        const user = new User(req.body);
        const userExists = await User.findOne({ 'email': user.email }).exec();
        if (userExists) {
            return res.status(400).json({ message: `User already exists` });
        }
        const salt = await bcrypt.genSalt(Number(process.env.SALT));
        user.password = await bcrypt.hash(user.password, salt);
        user.activation_token = crypto.randomBytes(12).toString('hex');

        await user.save();
        const clientUrl = process.env.CLIENT_URL;
        const link = `${clientUrl}/auth/verification/${user.activation_token}`;
        const mailOptions = {
            to: user.email,
            subject: "Please confirm your Email account",
            html: "Hello,<br> Please Click on the link to verify your email.<br><a href=" + link + ">Click here to verify</a>"
        }
        console.log(mailOptions);
        mailer.sendMail(mailOptions, function (mail_error, response) {
            if (mail_error) {
                console.log(mail_error);
            } else {
                console.log("Message sent");

            }
            return res.status(201).json({ message: 'Success, please check your email in order to activate account' });
        });

    } catch (error) {
        console.log(error);
    }
}

const loginUser = async (req, res) => {
    try {
        const authError = { message: 'Invalid username or password' };
        const { email, password } = new User(req.body);
        // check if user exists
        const user = await User.findOne({ email: email, active: true });
        if (!user) {
            return res.status(400).json(authError);
        }
        const isValid = await bcrypt.compare(password, user.password);
        if (!isValid) {
            return res.status(400).send(authError);
        }
        const accessToken = user.generateAccessToken(user);
        const refreshToken = user.generateRefreshToken(user);

        return res.status(201).json({
            access_token: accessToken,
            refresh_token: refreshToken,
            activation_token: user.activation_token,
            user_id: user.id,
            username: user.username
        });

    } catch (error) {
        console.log(error);
    }
}

const emailVerification = async (req, res) => {
    const activationToken = req.params.token;
    const user = await User.findOne({ activation_token: activationToken, active: false });
    if (!user) {
        return res.status(404).json({ message: template('user_not_found') });
    }
    user.active = true;
    await user.save();
    return res.status(200).json({ message: template('user_activated') });
}

const isTokenBlacklisted = async (token) => {
    try {
        const obj = await Blacklist.findOne({ token: token });
        if (obj && typeof (obj) === 'object') {
            return true;
        }
        return false;
    } catch (error) {
        console.log(error);
    }
}

const blockUser = async (userId) => {
    const user = await User.findById(userId);
    if(user.active === true) {
        user.active = false;
    }
   
    await user.save();
    return true;
}

const blacklistToken = async (token, user) => {
    try {
        const blacklist = new Blacklist({
            token: token,
            user_id: user.id
        });
        await blacklist.save();
        return true;
    } catch (error) {
        console.log(error)
    }
}


const refreshAccessToken = async (req, res) => {
    const authorization = req.header('authorization');
    const refreshToken = authorization.split(" ")[1];
    
    if (!refreshToken) {
        return res.status(403).send("Refresh token is required");
    }
    const {uid, type } = jwt.verify(refreshToken, process.env.JWT_KEY);
  
    if(type !== "refresh_token") {
        return res.status(403).send("Refresh token is invalid");
    }

    const user = await User.findById(uid);

    console.log(user);
    if (!user || !user.active) {
        return res.status(403).send("Invalid token");
    }

    const isBlacklisted = await isTokenBlacklisted(refreshToken);

    if (isBlacklisted) {
        await blockUser(uid);
        console.log(isBlacklisted);
        return res.status(403).send("User has been blocked");
    }

    const newAccessToken = user.generateAccessToken(user);
    const newRefreshToken = user.generateRefreshToken(user);

    await blacklistToken(refreshToken, user);

    return res.status(201).json({
        access_token: newAccessToken,
        refresh_token: newRefreshToken,
        user_id: user.id,
        username: user.username
    });
}


const forgotEmail = async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email, active: true });
        if (!user) {
            return res.status(200).json({ error: 'Email does not exists' });
        }
        const clientUrl = process.env.CLIENT_URL;
        const link = `${clientUrl}/password-reset/${user.activation_token}`;
        const mailOptions = {
            to: user.email,
            subject: `Password reset`,
            html: `Hello ${user.full_name},<br> Please Click on the link to reset your passwork.<br><a href="${link}">Click here to reset password</a>`
        }
        console.log('sending password reset email ...');
        console.log(mailOptions);
        mailer.sendMail(mailOptions, function (mail_error, response) {
            if (mail_error) {
                console.log(mail_error);
            } else {
                console.log("Message sent");

            }
            return res.status(200).json({ message: 'Your reset password email is heading your way.' });
        });

    } catch (error) {
        console.log(error);
        res.status(400).json({ error: error });
    }
}

const passwordResetForm = async (req, res) => {
    try {
        const activationToken = req.params.token;
        const user = await User.findOne({ activation_token: activationToken, active: true });
        if (!user) {
            return res.status(404).json({ message: template('user_not_found') });
        }
        return res.status(200).json({ activation_token: activationToken });
    } catch (error) {
        res.status(400).json({ error: error })
    }
}

const passwordReset = async (req, res) => {
    const newPassword = req.body.password;
    const newPasswordRepeat = req.body.password_repeat;
    if(newPassword.length < 6) {
        return res.status(200).json({ error: 'Pasword is to short' })
    }
    if(newPasswordRepeat !== newPassword) {
        return res.status(200).json({ error: 'Pasword and repeat password are not the same' })
    }
    
    const activationToken = req.body.token;
    if (newPassword !== newPasswordRepeat) {
        return res.status(400).json({ error: 'Pasword and repeat password are not the same' })
    }
    const user = await User.findOne({ activation_token: activationToken, active: true });
    if (!user) {
        return res.status(404).json({ message: template('user_not_found') });
    }
    const salt = await bcrypt.genSalt(Number(process.env.SALT));
    user.password = await bcrypt.hash(newPassword, salt);
    await user.save();
    return res.status(200).json({ message: 'Password has been successfully changed' })
}

module.exports = {
    createUser,
    loginUser,
    emailVerification,
    refreshAccessToken,
    forgotEmail,
    passwordResetForm,
    passwordReset
}

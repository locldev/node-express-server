const { User } = require('../models/user');

const getMyData = async (req, res) => {
    
    try {
        const id = req.body.id;
        if (!id.match(/^[0-9a-fA-F]{24}$/)) {
            return res.status(404).json({ message: 'User ID is not valid' })
          }

        const user = await User.findById(id);
        if (!user) {
            res.status(404).json({ message: 'User not found' })
        }
        const result = {
            id: user._id,
            username: user.username,
            email: user.email
        }
        return res.json(result);
    } catch (error) {
        console.log(error);
    }
}

module.exports = { getMyData }
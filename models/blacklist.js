const mongoose = require('mongoose');
const { Schema } = mongoose;

const BlacklistSchema = new Schema({
    token: { type: String, required: true },
    user_id: { type: String, required: true },
    created_at: {type: Date, default: Date.now()}
});


const Blacklist = mongoose.model('Blacklist', BlacklistSchema);
module.exports = Blacklist;
const mongoose = require('mongoose');
const { Schema } = mongoose;
const jwt = require('jsonwebtoken');
const Joi = require('joi');

const userShema = new Schema({
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    full_name: { type: String, required: true },
    activation_token: { type: String, required: true },
    active: { type: Boolean, default: false },
    role: { type: String, default: 'user' },
    blocked_at: {type: Date, default: null }
});

userShema.methods.generateAccessToken = (user) => {
    return jwt.sign({ uid: user.id, created_at: Date.now() }, process.env.JWT_KEY);
}

userShema.methods.generateRefreshToken = (user) => {
    return jwt.sign({ uid: user.id, type: "refresh_token", created_at: Date.now() }, process.env.JWT_KEY);
}

const validate = (user) => {
    const schema = Joi.object({
        username: Joi.string().alphanum().min(3).max(30).required().label('Username'),
        password: Joi.string().min(6).label('Password'),
        email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }).label('Email'),
        full_name: Joi.string().min(3).max(60).required().label('Full name')
    })
    return schema.validate(user);
}

const getUserById = (uid) => {
    console.log(uid);
    if(!uid) {
        return false;
    }
    const user = this.findById(uid);
    if(!user && user.active===false) {
        return false;
    }
    return user;
}

const User = mongoose.model('User', userShema);
module.exports = { User, validate, getUserById };
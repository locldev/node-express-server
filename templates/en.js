const translator = (key) => {
    const data=[];

    data['user_not_found'] = 'User was not found';
    data['user_activated'] = 'User acount is activated';

    return data[key] ?? key;
}

module.exports = translator;
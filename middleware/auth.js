const {getLogUserByToken, getAccessTokenCreatedTimestamp} = require('../services/auth');

module.exports = async (req, res, next) => {
    try {
         const user = await getLogUserByToken(req);
        if(user.error) {
            return res.status(403).json({error: user.error});
        }

        const tokenCreated = getAccessTokenCreatedTimestamp(req) + process.env.ACCESS_TOKEN_VALIDATION_MINUTES*60000;

        if(tokenCreated < Date.now()) {
            return res.status(401).send("Token has expired");
        }
        next();
    } catch (err) {
        console.log(err);
        res.status(400).send("Invalid token");

    }

}
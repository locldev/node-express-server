const mongoose = require('mongoose');

const db = async function main() {
  await mongoose.connect(process.env.DB, ()=>{
      console.log('Database is connected');
  });
}

module.exports = db;
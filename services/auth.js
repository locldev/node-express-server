const jwt = require('jsonwebtoken');
const { User } = require('../models/user');

const getAccessToken = (req) => {
    const authorization = req.header('authorization');
    const token = authorization.split(" ")[1];
    if (!token) {
        return { error: "Access denied" }
    }
    return token;
}


const getLogUserByToken = async (req) => {
    const token = getAccessToken(req);
    if (token.error) {
        return {error: token.error}
    }
    const { uid } = jwt.verify(token, process.env.JWT_KEY);
    const user = await User.findOne({ id: uid, active: true });
    if (!user) {
        return { error: "Access denied" }
    }
    return user;
}

const getAccessTokenCreatedTimestamp = (req) => {
    const token = getAccessToken(req);
    const { created_at } = jwt.verify(token, process.env.JWT_KEY);
    return created_at;
}


module.exports = {
    getAccessToken,
    getLogUserByToken,
    getAccessTokenCreatedTimestamp,
};
require('dotenv').config();

const express = require('express');
const cors = require('cors');
const conectToDb = require('./services/db');

const usersRouter = require('./routes/users')
const authRouter = require('./routes/auth')

// swagger
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const swaggerDefinition = {
    openapi: '3.0.0',
    info: {
      title: 'Express API',
      version: '1.0.0',
    },
  };
  const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: ['./routes/*.js'],
  };
const swaggerSpec = swaggerJSDoc(options);  


const app = express();
app.use(express.json());
app.use(cors());

app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);

app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

conectToDb();

const port = process.env.PORT || 8080;
app.listen(port, () => {
    console.log(`Server started on a port ${port}`);
});